# Portfolio

## Nederland
- ING
- TVM
- The Learning Network
- Belastingdienst
- Powerhouse
- ZPService
- GfK Daphne
- Reed Elsevier
- Avéro - Achmea | Achmea IT
- Euro Pool System
- Planbureau voor de Leefomgeving (PBL)
- Cito
- FBTO - Achmea
- Océ
- Rijks Instituut voor Volksgezondheid en Milieu (RIVM)
- Milieu en Natuur Planbureau (MNP)
- Rijksdienst voor het wegverkeer (RDW)
- British Telecom
- Koninklijke Nederlandse Organisatie van Verloskundigen (KNOV)
- RIVM-EARSS
- KNAW-NIWI

## Ierland & Engeland
- HSBC
- British Telecom
- HPI
- Agilent
- Sword EMC
