# Training & Coaching

## Training

Of het nu een introductie in Scrum of Kanban is, of een specifieke training 
een Agile practice (zoals bijvoorbeeld Test Driven Development) 
Peter brengt enthousiast de stof en en uitgebreide schat aan voorbeelden op 
tafel. 
  

## Coaching
Door samen op te werken met teams of ze vanaf de zijlijn te coachen brengt Peter
bestaande teams op een hoger niveau. 

