# Agile

Sinds eind 1999 is Peter bezig met werken volgens de Agile Principes. 
Eerst via de eXtreme Programming practices, later via Scrum & Kanban in kleine teams.
En weer later met LeSS grootschalig Agile met meerdere teams.

Via Agile practices  en methoden kan hij zijn beide passies echt naleven:
- leveren van __echte__ oplossingen,
- wat geleverd wordt werkt goed...niet maar half. 

Daarnaast vind Peter het erg belangrijk dat in Agile werken het 
__menselijke aspect__ wordt benadrukt: 
- open communicatie, 
- diversiteit aan karakters en disciplines, 
- samen staan voor wat je realiseert, 
- feedback zien als kans om te leren.

Peter zet vanuit zijn passie __hoog op kwaliteit__ in. 
Voortdurend wordt er getest. En zo efficient mogelijk door vergaande 
automatisering.  

Peter stimuleert teams steeds weer om de __gebruikers__ te vragen naar wat voor hen 
werkt. Zowel in de planvorming als door het continue laten beproeven 
van wat er gerealiseerd is.
