# Technicalities

## Talen
- Ruby
- Java 
- javascript
- Gherkin (beschrijven van testgevallen)
- Markdown (Wiki-opmaak)

## Raamwerken
- Ruby on Rails (web-applicatie ontwikkeling)
- Vue.js (web-frontend)  

## Tools
- Cucumber (BDD)
- Cypress (BDD) 
- GitLab / git 

## Concepten
- Test Driven Development 
- Behaviour Driven Development
- Object Oriented Programming
