---
home: true
heroImage: /hero.png
actionText: Mijn hoe en waarom →
actionLink: /content/agile.md
features:
- title: Agile
  details: >
    Peter leidt uw software ontwikkeling in goede banen met Agile 
    methoden en technieken als Scrum, Kanban en eXtreme Programming 
- title: Training & Coaching
  details: > 
    Zijn kennis en kunde draagt Peter graag over op uw teams. 
    Door training, coaching, presentaties en meewerken in projecten
- title: Technicalities
  details: >
    Peter programmeert met meerdere Talen en Frameworks voor het ontwikkelen van 
    software en het maken van Test Automation.
- title: Portfolio
  details: > 
    Een greep uit de organisaties voor wie Peter opdrachten heeft uitgevoerd in 
    Nederland, UK en Ierland                
- title: Werkt voor
  details: >
    Peter werkt bij De Agile Testers. Een groep Agile Nadenkers die 
    vanuit hun passie voor Kwaliteit elkaar scherp houden op alle 
    aspecten van Agile werken.   
- title: Contact
  details: > 
    (+31) 647 502 082      
    schrier.peter@gmail.com        
footer: Copyright © Peter Schrier 
---

#### Quicklinks:
[De Agile Testers](http://www.deagiletesters.nl)  
[Scrum](http://www.scrum.org)  
[LeSS](http://less.works)  


