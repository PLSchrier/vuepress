module.exports = {
  title: 'Peter Schrier',
  lang: ' nl-NL',
  description: 'Software development = Leveren met Kwaliteit',
  base: '/vuepress/',
  dest: 'public',
  themeConfig: {
    navbar: true,
    search: false,
    nav: [
      { text: 'Agile', link: '/content/agile.md' },
      { text: 'Training & Coaching', link: '/content/training_coaching.md' },
      { text: 'Portfolio', link: '/content/portfolio.md' },
      { text: 'Technicalities', link: '/content/technicalities.md' },
    ],
    sidebar: [
      ['/content/agile.md', 'Agile'],
      ['/content/training_coaching.md', 'Training & Coaching'],
      ['/content/portfolio.md', 'Portfolio'],
      ['/content/technicalities.md', 'Technicalities']
    ]
  }
}
